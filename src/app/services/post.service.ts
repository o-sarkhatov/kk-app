import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {IPostData, Post} from '../models/post';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private _URL = 'https://jsonplaceholder.typicode.com/posts';
  private _isValidated = false;

  constructor(private _http: HttpClient) {
  }

  public posts(): Observable<Post[]> {
    return this._http.get(this._URL).pipe(
      map((postData: IPostData[]) => postData.map(p => new Post(p)))
    );
  }
  public validate(): void {
    this._isValidated = true;
  }

  public isValidated(): boolean {
    return this._isValidated;
  }
}
