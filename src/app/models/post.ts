export class Post {
  id: number;
  title: string;
  constructor(data: IPostData) {
    this.id = data.id;
    this.title = data.title;
  }
}

export interface IPostData {
  userId: number;
  id: number;
  title: string;
  body: string;
}
