import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {Post} from '../../models/post';
import {PostService} from '../../services/post.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent {
  public form: FormGroup;
  public options: Observable<Post[]>;

  constructor(
    private _fb: FormBuilder,
    private _postService: PostService,
    private _router: Router) {
    this.form = this._fb.group({
      number: [null, [Validators.required, Validators.min(18), Validators.max(120)]],
      select: [null, Validators.required],
      check: [null, Validators.requiredTrue]
    });
    this.options = this._postService.posts();
  }

  public submit(): void {
    if (this.form.valid) {
      this._postService.validate();
      this._router.navigateByUrl('/final');
    } else {
      alert('Invalid form submitted!');
    }
  }

}
