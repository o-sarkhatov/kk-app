import { TestBed, inject } from '@angular/core/testing';

import { ValidateGuard } from './validate.guard';

describe('ValidateGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ValidateGuard]
    });
  });

  it('should ...', inject([ValidateGuard], (guard: ValidateGuard) => {
    expect(guard).toBeTruthy();
  }));
});
