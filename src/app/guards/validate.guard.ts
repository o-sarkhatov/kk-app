import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {PostService} from '../services/post.service';

@Injectable({
  providedIn: 'root'
})
export class ValidateGuard implements CanActivate {
  constructor(
    private _postService: PostService,
    private _router: Router
  ) {
  }

  canActivate(): boolean {
    if (this._postService.isValidated()) {
      return true;
    } else {
      this._router.navigateByUrl('');
      return false;
    }
  }

}
