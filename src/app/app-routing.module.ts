import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FormComponent} from './components/form/form.component';
import {FinalComponent} from './components/final/final.component';
import {ValidateGuard} from './guards/validate.guard';


const routes: Routes = [
  {path: '', component: FormComponent},
  {path: 'final', component: FinalComponent, canActivate: [ValidateGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
